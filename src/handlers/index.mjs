import { DynamoDBClient } from "@aws-sdk/client-dynamodb";
import { DynamoDBDocumentClient, ScanCommand } from "@aws-sdk/lib-dynamodb";
import { S3Client, PutObjectCommand } from "@aws-sdk/client-s3";

const client = new DynamoDBClient({});
const docClient = DynamoDBDocumentClient.from(client);
const s3 = new S3Client({
  region: "eu-central-1",
});

export const handler = async (event) => {
  // Log execution time
  console.log(Date.now());

  // Get current date for report name
  const currentDate = new Date();
  const year = currentDate.getFullYear();
  const month = (currentDate.getMonth() + 1).toString().padStart(2, "0");
  const reportName = `NEW_Trainers_Trainings_summary_${year}_${month}.csv`;
  const currentMonthString = new Date().toLocaleString("default", {
    month: "long",
  });

  try {
    // Scan dynamoDB table command
    const dbCommand = new ScanCommand({
      TableName: "TrainerTraineeTable",
    });

    // Execute command and get trainers
    const response = await docClient.send(dbCommand);
    const trainers = response.Items;

    // Filter out inactive trainers with training duration summary equals to 0
    const filteredTrainers = trainers.filter((trainer) => {
      return !(
        trainer.TraineeStatus === "Inactive" &&
        trainer.YearsList.find((y) => y.Year === year).Months.some(
          (m) =>
            m.Month === currentMonthString &&
            m.TrainingDurationSummary.TotalHours === 0
        )
      );
    });

    // Prepare CSV data
    const headers =
      "Trainer First Name,Trainer Last Name,Current month trainings duration summary time\n";
    const csvData = filteredTrainers
      .map((trainer) => {
        // Get current month training duration
        const currentYear = trainer.YearsList.find((y) => y.Year === year);
        const currentMonth = currentYear.Months.find(
          (m) => m.Month === currentMonthString
        );

        return `${trainer.TrainerFirstName},${trainer.TrainerLastName},${currentMonth.TrainingDurationSummary.TotalHours}`;
      })
      .join("\n");

    // Saving report
    const putCommand = new PutObjectCommand({
      Bucket: "jakub-marszal-web-app",
      Key: reportName,
      Body: headers + csvData,
      ContentType: "text/csv",
    });
    await s3.send(putCommand);

    return {
      statusCode: 200,
      body: JSON.stringify("Report generated and uploaded successfully"),
    };
  } catch (error) {
    console.error("Error:", error);
    return {
      statusCode: 500,
      body: JSON.stringify("Internal Server Error"),
    };
  }
};
