# Module 13: CI/CD tools

### Sub-task 1 – create and deploy SAM application

**1. Create a SAM application for lambda from the Lambda module**

![screenshot](screenshots/1.1.png)

**2. Install SAM CLI on your local machine**

![screenshot](screenshots/1.2.png)

**3. Execute sam deploy CLI command with appropriate flags to store the packaged lambda to S3 and deploy it with the appropriate resources**

![screenshot](screenshots/1.3.1.png)

![screenshot](screenshots/1.3.2.png)

**4. Open Lambda service and find in Functions and display deployed function**

![screenshot](screenshots/1.4.png)

**5. Trigger the recently deployed Lambda and ensure CSV report is present in S3**

![screenshot](screenshots/1.5.1.png)

![screenshot](screenshots/1.5.2.png)

### Sub-task 2 – build CI/CD pipelines for your services

**1. Create a CodeCommit repository**

![screenshot](screenshots/2.1.1.png)

![screenshot](screenshots/2.1.2.png)

**2. Create a CodeBuild spec**

![screenshot](screenshots/2.2.png)

**3. Create a CodeBuild project using AWS console**

![screenshot](screenshots/2.3.1.png)

![screenshot](screenshots/2.3.2.png)

![screenshot](screenshots/2.3.3.png)

![screenshot](screenshots/2.3.4.png)

![screenshot](screenshots/2.3.5.png)

![screenshot](screenshots/2.3.6.png)

**4. Create a CodePipeline project and configure source, build and deploy stage**

![screenshot](screenshots/2.4.1.png)

![screenshot](screenshots/2.4.2.png)

![screenshot](screenshots/2.4.3.png)

![screenshot](screenshots/2.4.4.png)

**5. Delete Lambda function resources created in previous sub-task**

![screenshot](screenshots/2.5.png)

**6. Make some changes to the Lambda function code and ensure the CodePipeline builds and deploys it**

![screenshot](screenshots/2.6.1.png)

![screenshot](screenshots/2.6.2.png)

**7. Trigger the recently deployed Lambda and ensure CSV report is present in S3.**

![screenshot](screenshots/2.7.1.png)

![screenshot](screenshots/2.7.2.png)
